package com.nc.onlinestore.service.mapper;

import com.nc.onlinestore.dto.OrderDto;
import com.nc.onlinestore.dto.PersonDto;
import com.nc.onlinestore.dto.ProductDto;
import com.nc.onlinestore.model.Order;
import com.nc.onlinestore.model.Person;
import com.nc.onlinestore.model.Product;
import org.aspectj.weaver.ast.Or;
import org.springframework.stereotype.Component;

@Component
public class OrderMapper implements Mapper<Order, OrderDto>{
    @Override
    public OrderDto toDto(Order entity) {
        return null;
    }

    @Override
    public Order toEntity(OrderDto dto) {
        return null;
    }

}
