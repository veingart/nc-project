package com.nc.onlinestore.service.mapper;

import com.nc.onlinestore.dto.PersonDto;
import com.nc.onlinestore.model.Person;
import org.springframework.stereotype.Component;

@Component
public class PersonMapper implements Mapper<Person, PersonDto> {
    @Override
    public PersonDto toDto(Person entity) {
        PersonDto personDto = new PersonDto();
        personDto.setId(entity.getId());
        personDto.setNamePerson(entity.getNamePerson());
        personDto.setSurnamePerson(entity.getSurnamePerson());
        personDto.setMailPerson(entity.getMailPerson());
        personDto.setUsername(entity.getUsername());
        personDto.setPassword(entity.getPassword());
        personDto.setActivationCode(entity.getActivationCode());
        personDto.setActive(entity.isActive());
        return personDto;
    }

    @Override
    public Person toEntity(PersonDto dto) {
        Person person = new Person();
        person.setId(dto.getId());
        person.setNamePerson(dto.getNamePerson());
        person.setSurnamePerson(dto.getSurnamePerson());
        person.setMailPerson(dto.getMailPerson());
        person.setLoginPerson(dto.getUsername());
        person.setLoginPerson(dto.getPassword());
        person.setActivationCode(dto.getActivationCode());
        person.setActive(dto.isActive());
        return null;
    }
}
