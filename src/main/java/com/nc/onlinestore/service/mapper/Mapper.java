package com.nc.onlinestore.service.mapper;

public interface Mapper<T, K> {

    K toDto(T entity);

    T toEntity(K dto);

}
