package com.nc.onlinestore.service.mapper;

import com.nc.onlinestore.dto.PhotoDTO;
import com.nc.onlinestore.model.Photo;
import org.springframework.security.core.parameters.P;

public class PhotoMapper implements Mapper<Photo, PhotoDTO>{
    @Override
    public PhotoDTO toDto(Photo entity) {
        PhotoDTO photoDTO = new PhotoDTO();
        photoDTO.setId(entity.getId());
        photoDTO.setTitle(entity.getTitle());
        photoDTO.setImage(entity.getImage());
        return photoDTO;
    }

    @Override
    public Photo toEntity(PhotoDTO dto) {
        return null;
    }

}
