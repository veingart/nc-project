package com.nc.onlinestore.service.mapper;

import com.nc.onlinestore.dto.CategoryDto;
import com.nc.onlinestore.dto.CharacteristicDto;
import com.nc.onlinestore.model.Category;
import com.nc.onlinestore.model.Characteristic;
import org.springframework.stereotype.Component;

@Component
public class CharacteristicMapper implements Mapper<Characteristic, CharacteristicDto> {
    @Override
    public CharacteristicDto toDto(Characteristic entity) {
        CharacteristicDto characteristicDto = new CharacteristicDto();
        characteristicDto.setId(entity.getId());
        characteristicDto.setNameCharacteristic(entity.getNameCharacteristic());
        CategoryMapper categoryMapper = new CategoryMapper();
        CategoryDto categoryDto = categoryMapper.toDto(entity.getCategory());
        characteristicDto.setCategory(categoryDto);
        return characteristicDto;
    }

    @Override
    public Characteristic toEntity(CharacteristicDto dto) {
        Characteristic characteristic = new Characteristic();
        characteristic.setId(dto.getId());
        characteristic.setNameCharacteristic(dto.getNameCharacteristic());
        CategoryMapper categoryMapper = new CategoryMapper();
        Category category = categoryMapper.toEntity(dto.getCategory());
        characteristic.setCategory(category);
        return characteristic;
    }
}
