package com.nc.onlinestore.service.mapper;

import com.nc.onlinestore.dto.CategoryDto;
import com.nc.onlinestore.model.Category;
import org.springframework.stereotype.Component;

@Component
public class CategoryMapper implements Mapper<Category, CategoryDto> {
    @Override
    public CategoryDto toDto(Category entity) {
        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setId(entity.getId());
        categoryDto.setNameCategory(entity.getNameCategory());
        return categoryDto;
    }

    @Override
    public Category toEntity(CategoryDto dto) {
        Category category = new Category();
        category.setId(dto.getId());
        category.setNameCategory(dto.getNameCategory());
        return category;
    }
}
