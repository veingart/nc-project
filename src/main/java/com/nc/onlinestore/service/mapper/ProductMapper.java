package com.nc.onlinestore.service.mapper;

import com.nc.onlinestore.dto.CategoryDto;
import com.nc.onlinestore.dto.PhotoDTO;
import com.nc.onlinestore.dto.ProductDto;
import com.nc.onlinestore.model.Category;
import com.nc.onlinestore.model.Photo;
import com.nc.onlinestore.model.Product;
import com.nc.onlinestore.repository.CategoryRepository;
import com.nc.onlinestore.service.Impl.CategoryServiceImpl;
import org.springframework.stereotype.Component;

@Component
public class ProductMapper implements Mapper<Product, ProductDto> {

    private CategoryMapper categoryMapper;
    private CategoryRepository categoryRepository;

    @Override
    public ProductDto toDto(Product entity) {
        ProductDto productDto = new ProductDto();
        productDto.setId(entity.getId());
        productDto.setNameProduct(entity.getNameProduct());
        productDto.setDescriptionProduct(entity.getDescriptionProduct());
        productDto.setPriceProduct(entity.getPriceProduct());
        productDto.setCountProduct(entity.getCountProduct());
        CategoryDto category;
        category = categoryMapper.toDto(entity.getCategory());
        productDto.setCategory(category);
        PhotoMapper photoMapper = new PhotoMapper();
        PhotoDTO photoDTO = photoMapper.toDto(entity.getPhoto());
        productDto.setPhoto(photoDTO);
        return productDto;
    }

    @Override
    public Product toEntity(ProductDto dto) {
        Product product = new Product();
        product.setId(dto.getId());
        product.setNameProduct(dto.getNameProduct());
        product.setDescriptionProduct(dto.getDescriptionProduct());
        product.setPriceProduct(dto.getPriceProduct());
        product.setCountProduct(dto.getCountProduct());
        Category category;
        category = categoryMapper.toEntity(dto.getCategory());
        product.setCategory(category);
        return product;
    }

}
