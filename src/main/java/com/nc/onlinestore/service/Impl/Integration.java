package com.nc.onlinestore.service.Impl;

import com.nc.onlinestore.dto.ExecutorDto;
import com.nc.onlinestore.dto.OrderDto;
import com.nc.onlinestore.dto.ServiceDto;
import com.nc.onlinestore.enums.Promo;
import com.nc.onlinestore.model.Order;
import com.nc.onlinestore.model.Person;
import org.apache.http.HttpEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

import java.net.ConnectException;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;

public class Integration {

    private static Person person;
    private static final Logger log = LoggerFactory.getLogger(Currency.class);

    public static HashSet getPromo(String email) {
        log.info("Taking promo by user");
        RestTemplate restTemplate = new RestTemplate();
        HashSet promos;
        try{
            String url = "https://hip-hope.ml:8080/api/users";
            promos = restTemplate.getForObject(url + "/" + email + "/promocodes",HashSet.class);
        } catch (Exception e){
            promos = null;
        }

        return promos;
    }

    public static List<ExecutorDto> getExecutors(){
        List <ExecutorDto> executors;
        RestTemplate restTemplate = new RestTemplate();
        try{
            String url = "http://192.168.56.101:7001/api/executor";
            executors = restTemplate.getForObject(url, List.class);
        } catch (Exception e){
            executors = null;
        }
        return executors;
    }

    public static List<ServiceDto> getServices(){
        List <ServiceDto> services;
        RestTemplate restTemplate = new RestTemplate();
        try{
            String url = "http://192.168.56.101:7001/api/services";
            services = restTemplate.getForObject(url, List.class);
        } catch (Exception e){
            services = null;
        }
        return services;
    }
}
