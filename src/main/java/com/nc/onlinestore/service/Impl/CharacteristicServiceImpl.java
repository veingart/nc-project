package com.nc.onlinestore.service.Impl;

import com.nc.onlinestore.model.Characteristic;
import com.nc.onlinestore.model.CharacteristicForm;
import com.nc.onlinestore.model.CharacteristicValue;
import com.nc.onlinestore.model.Product;
import com.nc.onlinestore.repository.CharacteristicRepository;
import com.nc.onlinestore.repository.CharacteristicValueRepository;
import com.nc.onlinestore.repository.ProductRepository;
import com.nc.onlinestore.service.Interfaces.CharacteristicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class CharacteristicServiceImpl implements CharacteristicService {

    private final CharacteristicRepository characteristicRepository;
    private final ProductRepository productRepository;
    private final CharacteristicValueRepository characteristicValueRepository;

    @Autowired
    public CharacteristicServiceImpl(CharacteristicRepository characteristicRepository, ProductRepository productRepository, CharacteristicValueRepository characteristicValueRepository) {
        this.characteristicRepository = characteristicRepository;
        this.productRepository = productRepository;
        this.characteristicValueRepository = characteristicValueRepository;
    }

    @Override
    public List<Characteristic> findAll() {
        return characteristicRepository.findAll();
    }

    @Override
    public List<CharacteristicForm> findAllCharacteristic() {
        List<CharacteristicForm> forms = new ArrayList<>();
        List<Characteristic> characteristics = characteristicRepository.findAll();
        for (int i = 0; i < characteristics.size(); i++) {
            CharacteristicForm characteristicForm = new CharacteristicForm();
            Characteristic characteristic = characteristics.get(i);
            // List<CharacteristicValue> values = characteristicValueRepository.findAllByCharacteristic(characteristic);
            List<String> characteristicValues = new ArrayList<>();
          /*  for (int b = 0; b<values.size();i++){
                CharacteristicValue characteristicValue = values.get(b);
                characteristicValues.add(characteristicValue.getValueCharacteristic());
            }*/
            characteristicForm.setId(characteristic.getId());
            characteristicForm.setNameCharacteristic(characteristic.getNameCharacteristic());
         //   characteristicForm.setCharacteristicValues(characteristicValues);
            characteristicForm.setNameCategory(characteristic.getCategory().getNameCategory());
            forms.add(characteristicForm);
        }

        return forms;
    }

    @Override
    public List<CharacteristicValue> findAllValues() {
        List<CharacteristicValue> characteristics = characteristicValueRepository.findAll();
        return characteristics;
    }

    @Override
    public Optional<Characteristic> findById(long id) {
        return characteristicRepository.findById(id);
    }


    @Override
    public Characteristic save(Characteristic characteristic) {
        characteristicRepository.save(characteristic);
        return characteristic;
    }

    @Override
    public CharacteristicValue findByCharacteristicValue(String characteristicValue) {
        return characteristicValueRepository.findByValueCharacteristic(characteristicValue);
    }

    @Override
    public CharacteristicValue update(long characteristicId, CharacteristicValue newCharacteristic) {
        CharacteristicValue oldCharacteristic = characteristicValueRepository.findById(characteristicId);
        if (newCharacteristic.getCharacteristic().getCategory().getId() == -1) {
            newCharacteristic.getCharacteristic().setCategory(oldCharacteristic.getCharacteristic().getCategory());
            characteristicValueRepository.save(newCharacteristic);
        } else {
            characteristicRepository.save(newCharacteristic.getCharacteristic());
            characteristicValueRepository.save(newCharacteristic);
        }
        return newCharacteristic;
    }

    @Override
    public void delete(long id) {
        CharacteristicValue characteristicValue = characteristicValueRepository.findById(id);

        List<CharacteristicValue> values = characteristicValueRepository.findAllByCharacteristic(characteristicValue.getCharacteristic());
        if (values.size() == 1) {
            characteristicRepository.deleteById(characteristicValue.getCharacteristic().getId());
        } else {
            characteristicValue.setCharacteristic(null);
        }
        characteristicValueRepository.deleteById(id);
    }

    @Override
    public List<Characteristic> findDistinctByCategoryId(long id) {
        return characteristicRepository.findDistinctByCategoryId(id);
    }

    @Override
    public Set<List<Characteristic>> characteristicsCreate(List<Characteristic> characteristics) {
        Set<List<Characteristic>> characts = new LinkedHashSet<>();
        for (int i = 0; i < characteristics.size(); i++) {
            String nameChar = characteristics.get(i).getNameCharacteristic();
            characts.add(characteristics.stream().filter(item -> item.getNameCharacteristic()
                    .equals(nameChar)).collect(Collectors.toList()));
        }
        return characts;
    }

    @Override
    public CharacteristicValue findValueById(long id) {
        return characteristicValueRepository.findById(id);
    }

    @Override
    public CharacteristicValue saveValue(CharacteristicValue characteristicValue) {
        return characteristicValueRepository.save(characteristicValue);
    }

    @Override
    public List<CharacteristicValue> saveAllValue(List<CharacteristicValue> characteristicValue) {
        return characteristicValueRepository.saveAll(characteristicValue);
    }

    @Override
    public List<CharacteristicValue> findAllByCharacteristicCategoryId(long id) {
        return characteristicValueRepository.findAllByCharacteristicCategoryId(id);
    }

    @Override
    public List<CharacteristicForm> findForFilter(long id) {
        List<CharacteristicForm> characteristicForms = new ArrayList<>();
        List<Characteristic> characteristics = characteristicRepository.findAllByCategoryId(id);
        long idChar = 1;
        for (int i = 0; i < characteristics.size(); i++) {
            CharacteristicForm characteristicForm = new CharacteristicForm();
            characteristicForm.setId(idChar);
            characteristicForm.setNameCategory(characteristics.get(i).getCategory().getNameCategory());
            characteristicForm.setNameCharacteristic(characteristics.get(i).getNameCharacteristic());
            characteristicForm.setCharacteristicValues(characteristicValueRepository.findAllByCharacteristic(characteristics.get(i)));
            characteristicForms.add(characteristicForm);
        }
        return characteristicForms;
    }

}
