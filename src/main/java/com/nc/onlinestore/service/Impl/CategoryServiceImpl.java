package com.nc.onlinestore.service.Impl;

import com.nc.onlinestore.model.Category;
import com.nc.onlinestore.model.Characteristic;
import com.nc.onlinestore.model.Product;
import com.nc.onlinestore.repository.CategoryRepository;
import com.nc.onlinestore.repository.CharacteristicRepository;
import com.nc.onlinestore.repository.ProductRepository;
import com.nc.onlinestore.service.Interfaces.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository categoryRepository;
    private final ProductRepository productRepository;
    private final CharacteristicRepository characteristicRepository;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository, ProductRepository productRepository, CharacteristicRepository characteristicRepository) {
        this.categoryRepository = categoryRepository;
        this.productRepository = productRepository;
        this.characteristicRepository = characteristicRepository;
    }


    @Override
    public List<Category> findAll() {
        return categoryRepository.findAll();
    }

    @Override
    public Optional<Category> findById(long idCategory) {
        return categoryRepository.findById(idCategory);
    }

    @Override
    public Category save(Category category) {
        return categoryRepository.save(category);
    }

    @Override
    public Category update(long categoryId, Category newCategory) {
         categoryRepository.findById(categoryId).map(category -> {
            category.setNameCategory(newCategory.getNameCategory());
            return categoryRepository.save(category);
         });
         return newCategory;
    }

    @Override
    public void delete(long id) {
        List<Product> products = productRepository.findByCategoryId(id);
        products.forEach(hardware -> hardware.setCategory(null));
        productRepository.saveAll(products);
        List<Characteristic> characteristics = characteristicRepository.findAllByCategoryId(id);
        characteristics.forEach(characteristic -> characteristic.setCategory(null));
        categoryRepository.deleteById(id);
    }
}
