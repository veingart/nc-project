package com.nc.onlinestore.service.Impl;

import com.nc.onlinestore.enums.Promo;
import com.nc.onlinestore.model.*;
import com.nc.onlinestore.repository.CommentRepository;
import com.nc.onlinestore.repository.OrderRepository;
import com.nc.onlinestore.repository.PhotoRepository;
import com.nc.onlinestore.repository.ProductRepository;
import com.nc.onlinestore.service.Interfaces.PhotoService;
import com.nc.onlinestore.service.Interfaces.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {


    private final ProductRepository productRepository;
    private final PhotoService photoService;
    private final PhotoRepository photoRepository;
    private final OrderRepository orderRepository;
    private final CommentRepository commentRepository;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository, PhotoService photoService, PhotoRepository photoRepository, OrderRepository orderRepository, CommentRepository commentRepository) {
        this.productRepository = productRepository;
        this.photoService = photoService;
        this.photoRepository = photoRepository;
        this.orderRepository = orderRepository;
        this.commentRepository = commentRepository;
    }


    @Override
    public List<Product> findAll() {
        List<Product> products = productRepository.findAll();
        Product product =  productRepository.findByPromo(Promo.FREEMP3);
        products.remove(product);
        return products;
    }

    @Override
    public List<Product> findAllforAdmin() {
        List<Product> products = productRepository.findAll();
        return products;
    }

    @Override
    public List<Product> findTop() {
        ProductRatingComparator productRatingComparator = new ProductRatingComparator();
        List<Product> products = productRepository.findAll();
        Product product =  productRepository.findByPromo(Promo.FREEMP3);
        products.remove(product);
        products.sort(productRatingComparator);
        List<Product> top = new ArrayList<>();
        int size = 0;
        if(products.size()<11) {
            size = products.size();
        } else {size = 11;}
        for (int i = 0; i<size; i++) {
            top.add(products.get(i));
        }
        return top;
    }

    @Override
    public Product findById(long id) {
        Product product = productRepository.findById(id).get();
        return product;
    }

    @Override
    public Product findByPromo(Promo promo){
        return productRepository.findByPromo(promo);
    }

    @Override
    public Product update(long productId, Product newProduct) {
        productRepository.findById(productId).map(product -> {
            product.setNameProduct(newProduct.getNameProduct());
            product.setDescriptionProduct(newProduct.getDescriptionProduct());
            product.setCountProduct(newProduct.getCountProduct());
            product.setPriceProduct(newProduct.getPriceProduct());
            product.setCategory(newProduct.getCategory());
            product.setCharacteristics(newProduct.getCharacteristics());
            return productRepository.save(product);
        });
        return newProduct;
    }

    @Override
    public Product save(Product product) {
        productRepository.save(product);
        return product;
    }

    @Override
    public void addNewProduct(ProductForm productForm) throws IOException {
        Product product = new Product();
        product.setNameProduct(productForm.getNameProduct());
        product.setCategory(productForm.getCategory());
        product.setDescriptionProduct(productForm.getDescriptionProduct());
        product.setCharacteristics(productForm.getCharacteristics());
        product.setPriceProduct(Double.parseDouble(productForm.getPriceProduct()));
        product.setCountProduct(productForm.getCountProduct());
        product.setPerson(productForm.getPerson());
        product.setRating(0);
        Photo photo = photoService.uploadPhoto(productForm.getPhotoUpload(), product);
        if (photo == null) {
            Photo defaultPhoto = photoRepository.findByTitle("default");
            product.setPhoto(defaultPhoto);
        } else {
            product.setPhoto(photo);
        }
        save(product);

    }

    @Override
    public void delete(Long id) {
        Product product = productRepository.findById(id).get();
        product.setCharacteristics(null);
        productRepository.save(product);
        Order order = orderRepository.findByProductId(id);
        if (order != null) {
            orderRepository.deleteById(order.getId());
        }
        productRepository.deleteById(id);
    }

    @Override
    public void editProduct(ProductForm productForm) {
        Product oldProduct = productRepository.findById(productForm.getId()).get();
        Product newProduct = new Product();
        newProduct.setId(productForm.getId());
        newProduct.setNameProduct(productForm.getNameProduct());
        newProduct.setDescriptionProduct(productForm.getDescriptionProduct());
        if (productForm.getCategory().getId() == -1)
            newProduct.setCategory(oldProduct.getCategory());
        else
            newProduct.setCategory(productForm.getCategory());

        if (productForm.getCharacteristics() == null)
            newProduct.setCharacteristics(oldProduct.getCharacteristics());
        else
            newProduct.setCharacteristics(productForm.getCharacteristics());

        newProduct.setPriceProduct(Double.parseDouble(productForm.getPriceProduct()));
        newProduct.setCountProduct(productForm.getCountProduct());
        newProduct.setPerson(oldProduct.getPerson());
        save(newProduct);
    }

    @Override
    public List<Product> findByCategory(long id) {
        List<Product> products = productRepository.findByCategoryId(id);
        Product product =  productRepository.findByPromo(Promo.FREEMP3);
        products.remove(product);
        return products;
    }

    @Override
    public List<Product> filter(List<CharacteristicValue> characteristics, long id) {
        List<Product> products = productRepository.findByCharacteristicsInAndCategoryId(characteristics, id);
        Product product =  productRepository.findByPromo(Promo.FREEMP3);
        products.remove(product);
        return products;

    }

    @Override
    public List<Product> findBycharacteristics(List<Characteristic> characteristics) {
        List<Product> products = productRepository.findByCharacteristicsIn(characteristics);
        Product product =  productRepository.findByPromo(Promo.FREEMP3);
        products.remove(product);
        return products;
    }

    @Override
    public Product setNewRating(Product product) {
        List<Comment> comments = commentRepository.findAllByProduct(product);
        List<Integer> ratings = new ArrayList<>();
        for (int i = 0; i < comments.size(); i++) {
            ratings.add(comments.get(i).getRating());
        }
        double sum = 0;
        for (int i = 0; i < ratings.size(); i++) {
            sum += ratings.get(i);
        }
        if (ratings.size() != 0) {
            product.setRating(sum / ratings.size());
        } else product.setRating(0);

        save(product);
        return product;
    }

    public Page<Product> findPaginatedProduct(Pageable pageable, List<Product> products){
        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<Product> list;
        if (products.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, products.size());
            list = products.subList(startItem, toIndex);
        }

        Page<Product> bookPage
                = new PageImpl<Product>(list, PageRequest.of(currentPage, pageSize), products.size());

        return bookPage;
    }
}
