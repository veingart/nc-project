package com.nc.onlinestore.service.Impl;

import com.nc.onlinestore.model.Comment;
import com.nc.onlinestore.model.Person;
import com.nc.onlinestore.model.Product;
import com.nc.onlinestore.repository.CommentRepository;
import com.nc.onlinestore.service.Interfaces.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CommentServiceImpl implements CommentService {

    private final CommentRepository commentRepository;

    @Autowired
    public CommentServiceImpl(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }


    @Override
    public Optional<Comment> findById(long id) {
        return commentRepository.findById(id);
    }

    @Override
    public List<Comment> findByPerson(Person person) {
        return commentRepository.findAllByPerson(person);
    }

    @Override
    public List<Comment> findByProduct(Product product) {
        return commentRepository.findAllByProduct(product);
    }

    @Override
    public Comment save(Comment comment) {
        return commentRepository.save(comment);
    }

    @Override
    public Comment update(Comment comment) {
        return commentRepository.save(comment);
    }

    @Override
    public void delete(long id) {
        commentRepository.deleteById(id);
    }
}
