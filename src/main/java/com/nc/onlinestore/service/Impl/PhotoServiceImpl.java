package com.nc.onlinestore.service.Impl;

import com.cloudinary.Singleton;
import com.cloudinary.utils.ObjectUtils;
import com.nc.onlinestore.model.Photo;
import com.nc.onlinestore.model.PhotoUpload;
import com.nc.onlinestore.model.Product;
import com.nc.onlinestore.repository.PhotoRepository;
import com.nc.onlinestore.service.Interfaces.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Service
public class PhotoServiceImpl implements PhotoService {

    private final PhotoRepository photoRepository;

    @Autowired
    public PhotoServiceImpl(PhotoRepository photoRepository) {
        this.photoRepository = photoRepository;
    }


    @Override
    public List<Photo> listPhotos() {
        return photoRepository.findAll();
    }

    @Override
    public Photo uploadPhoto(PhotoUpload photoUpload, Product product) throws IOException {
        Map params = ObjectUtils.asMap(
                "cloud_name", "elmart",
                "api_key", "738356442926326",
                "api_secret", "Lyd5obcbKq-OJs36o6EpLWB4fhw");
        Map uploadResult = null;
        if (photoUpload.getFile() != null && !photoUpload.getFile().isEmpty()) {
            uploadResult = Singleton.getCloudinary().uploader().upload(photoUpload.getFile().getBytes(),
                    params);
            photoUpload.setPublicId((String) uploadResult.get("public_id"));
            Object version = uploadResult.get("version");
            if (version instanceof Integer) {
                photoUpload.setVersion(new Long((Integer) version));
            } else {
                photoUpload.setVersion((Long) version);
            }

            photoUpload.setSignature((String) uploadResult.get("signature"));
            photoUpload.setFormat((String) uploadResult.get("format"));
            photoUpload.setResourceType((String) uploadResult.get("resource_type"));
            Photo photo = new Photo();
            photo.setUpload(photoUpload);
            save(photo);
            return photo;
        }
        return null;
    }

    @Override
    public void deletePhoto(Photo photo){
        photoRepository.delete(photo);
    }

    @Override
    public void save(Photo photo){
        photoRepository.save(photo);
    }


}
