package com.nc.onlinestore.service.Impl;

import com.nc.onlinestore.enums.Promo;
import com.nc.onlinestore.enums.Status;
import com.nc.onlinestore.model.Order;
import com.nc.onlinestore.model.OrderArchive;
import com.nc.onlinestore.model.Person;
import com.nc.onlinestore.model.Product;
import com.nc.onlinestore.repository.OrderArchiveRepository;
import com.nc.onlinestore.repository.OrderRepository;
import com.nc.onlinestore.repository.ProductRepository;
import com.nc.onlinestore.service.Interfaces.OrderService;
import com.nc.onlinestore.service.Interfaces.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;
    private final PersonService personService;
    private final ProductRepository productRepository;
    private final OrderArchiveRepository orderArchiveRepository;

    @Autowired
    public OrderServiceImpl(OrderRepository orderRepository, PersonService personService, ProductRepository productRepository, OrderArchiveRepository orderArchiveRepository) {
        this.orderRepository = orderRepository;
        this.personService = personService;
        this.productRepository = productRepository;
        this.orderArchiveRepository = orderArchiveRepository;
    }

    @Override
    public List<Order> findAll() {
        return orderRepository.findAll();
    }

    @Override
    public Optional<Order> findById(long id) {
        return orderRepository.findById(id);
    }

    @Override
    public Order save(Order order) {
        orderRepository.save(order);
        return order;
    }

    @Override
    public Order update(long orderId, Order newOrder) {
        orderRepository.findById(orderId).map(order -> {
            order.setCountOrder(newOrder.getCountOrder());
            return orderRepository.save(order);
        });
        return newOrder;
    }

    @Override
    public boolean createOneOrderForAll(long productId, int count) {
        Product product = productRepository.findById(productId).orElse(new Product());
        Person person = (Person) personService.findAuthenticationPerson();
        Order orderExist = orderRepository.findByProductAndPerson(product, person);
        if (orderExist != null && orderExist.getCountOrder() >= product.getCountProduct())
            return false;
        if (product.getCountProduct() == 0) {
            return false;
        } else {
            if (orderExist != null) {
                orderExist.setCountOrder(orderExist.getCountOrder() + count);
                orderRepository.save(orderExist);
            } else {
                Order order = new Order();
                order.setProduct(product);
                order.setCountOrder(count);
                order.setPerson(person);
                order.setStatus(Status.IN_CART);
                orderRepository.save(order);
            }
            return true;
        }
    }

    @Override
    public void delete(Long id) {
        Order order = orderRepository.findById(id).get();
        if (order.getStatus().equals(Status.IN_PROCESSING) || order.getStatus().equals(Status.EXECUTED) || order.getStatus().equals(Status.CLOSED)) {
            Product product = productRepository.findById(order.getProduct().getId()).get();
            product.setCountProduct(product.getCountProduct() + order.getCountOrder());
        }
        orderRepository.deleteById(id);
    }

    @Override
    public List<Order> findByPerson(Person person) {
        return orderRepository.findByPerson(person);
    }


    @Override
    public void changeStatusOnClosed(long id) {
        Order order = orderRepository.findById(id).get();
        order.setStatus(Status.CLOSED);
        orderRepository.save(order);
        OrderArchive orderArchive = new OrderArchive();
        orderArchive.setProduct(order.getProduct());
        orderArchive.setCountOrder(order.getCountOrder());
        orderArchive.setPerson(order.getPerson());
        orderArchive.setStatus(Status.ARCHIVED);
        orderArchiveRepository.save(orderArchive);
    }

    @Override
    public void changeStatusOnExecuted(long id) {
        Order order = orderRepository.findById(id).get();
        order.setStatus(Status.EXECUTED);
        orderRepository.save(order);
    }

    @Override
    public List<Order> findByPersonAndStatus(Person person, Status status) {
        return (List<Order>) orderRepository.findByPersonAndStatus(person, status);
    }

    @Override
    public List<OrderArchive> findByArchive(Person person) {
        return (List<OrderArchive>) orderArchiveRepository.findByPerson(person);
    }

    @Override
    public double createTotalCost(List<Order> orders) {
        return orders.stream().mapToDouble(item -> item.getProduct().getPriceProduct() * item.getCountOrder()).sum();

    }

    @Override
    public void deleteAllByPersonId(long id) {
        orderRepository.deleteAllByPersonId(id);
    }

    @Override
    public boolean check() {
        boolean isAllValidate = true;
        Person person = (Person) personService.findAuthenticationPerson();
        List<Order> orders = orderRepository.findByPersonAndStatus(person, Status.IN_CART);
        Set<Promo> promos = new HashSet<>();
        orders.forEach(order -> order.setStatus(Status.IN_PROCESSING));
        orders.forEach(order -> {
            if (order.getCountOrder() <= order.getProduct().getCountProduct()) {
                Product product = order.getProduct();
                person.getPromos().add(product.getPromo());
                product.setCountProduct(order.getProduct().getCountProduct() - order.getCountOrder());
            } else
                order.setCountOrder(0);
        });
        List<Order> deleteOrders = orders.stream().filter(order -> order.getCountOrder() == 0).collect(Collectors.toList());
        if (deleteOrders.size() != 0) {
            orderRepository.deleteAll(deleteOrders);
            isAllValidate = false;
        }
        orders.removeIf(order -> order.getCountOrder() == 0);
        List<Product> products = orders.stream().map(Order::getProduct).collect(Collectors.toList());
        personService.save(person);
        productRepository.saveAll(products);
        orderRepository.saveAll(orders);
        return isAllValidate;
    }

    @Override
    public List<Order> findAllForAdmin() {
        List<Order> orders = orderRepository.findByStatus(Status.IN_PROCESSING);
        List<Order> exec = orderRepository.findByStatus(Status.EXECUTED);
        List<Order> closed = orderRepository.findByStatus(Status.CLOSED);
        orders.addAll(exec);
        orders.addAll(closed);
        return orders;
    }

    @Override
    public List<Order> findAllForManager(Person person) {
        List<Order> orders = orderRepository.findOrderByProduct_PersonAndStatus(person, Status.IN_PROCESSING);
        List<Order> exec = orderRepository.findOrderByProduct_PersonAndStatus(person, Status.EXECUTED);
        List<Order> closed = orderRepository.findOrderByProduct_PersonAndStatus(person, Status.CLOSED);
        orders.addAll(exec);
        orders.addAll(closed);
        return orders;
    }

    @Override
    public Page<Order> findPaginatedOrder(Pageable pageable, List<Order> orders){
        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<Order> list;
        if (orders.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, orders.size());
            list = orders.subList(startItem, toIndex);
        }

        Page<Order> orderPage
                = new PageImpl<Order>(list, PageRequest.of(currentPage, pageSize), orders.size());

        return orderPage;
    }
}