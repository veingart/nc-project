package com.nc.onlinestore.service.Impl;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

import java.util.LinkedHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Currency {
    private static double result;
    private static final Logger log = LoggerFactory.getLogger(Currency.class);


    public static double getDollarCurrency() {
        log.info("Getting the currency value(" + result + ").");
        return result;
    }

    public static void startSchedule() {
        log.info("Running a timer for currency updates.");
        ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
        service.scheduleAtFixedRate(com.nc.onlinestore.service.Impl.Currency::updateCurrencyFromNBRBAPI,
                12, 12, TimeUnit.HOURS);
    }

    public static void updateCurrencyFromNBRBAPI() {
        log.info("Taking currency value from api nat. Bank of Belarus.");
        RestTemplate restTemplate = new RestTemplate();
        LinkedHashMap currency = restTemplate.getForObject(
                "https://www.nbrb.by/api/exrates/rates/840?parammode=1", LinkedHashMap.class);
        result = Double.parseDouble(currency.get("Cur_OfficialRate").toString());
    }
}

