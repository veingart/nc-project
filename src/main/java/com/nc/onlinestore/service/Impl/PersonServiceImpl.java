package com.nc.onlinestore.service.Impl;

import com.nc.onlinestore.config.SpringSecurityConfig;
import com.nc.onlinestore.controller.restController.PersonController;
import com.nc.onlinestore.enums.Role;
import com.nc.onlinestore.model.Person;
import com.nc.onlinestore.repository.PersonRepository;
import com.nc.onlinestore.service.Interfaces.PersonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

import static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4;

@Service
public class PersonServiceImpl implements PersonService {

    private static final Logger log = LoggerFactory.getLogger(PersonController.class);

    private final PersonRepository personRepository;
    private final MailSender mailSender;
    private final SpringSecurityConfig springSecurityConfig;

    @Autowired
    public PersonServiceImpl(PersonRepository personRepository, MailSender mailSender, SpringSecurityConfig springSecurityConfig) {
        this.personRepository = personRepository;
        this.mailSender = mailSender;
        this.springSecurityConfig = springSecurityConfig;
    }

    @Override
    public List<Person> findAll() {
        return personRepository.findAll();
    }

    @Override
    public Person findById(long id) {
        return personRepository.findById(id);
    }

    @Override
    public Person findByMail(String email) {
        return personRepository.findByMailPerson(email);
    }

    @Override
    public Person save(Person person) {
        personRepository.save(person);
        return person;
    }

    @Override
    public Person update(Person newPerson) {
        Person oldPerson = personRepository.findById(newPerson.getId());
        newPerson.setActive(oldPerson.isActive());
        newPerson.setRole(oldPerson.getRole());
        return personRepository.save(newPerson);
    }

    @Override
    public void delete(Long id) {
        personRepository.deleteById(id);
    }

    @Override
    public UserDetails findAuthenticationPerson() {
        log.info("Finding an authorized user.");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return personRepository.findByLoginPerson(auth.getName());
    }

    @Override
    public List<Person> findPersonsForAdmin() {
        List<Person> people = personRepository.findAll();
        people.removeIf(person -> person.getRole().equals(Role.ADMIN));
        log.info("Taking data from the database (All users except administrators).");
        return people;
    }

    @Override
    public boolean confirmNewUser(Person person) {
        String urlAddress = "34.121.1.45";
        String message = String.format(
                "Hello, %s! \n" +
                        "Welcome to localhost. Please, visit next link: http://%s:8050/activate/%s",
                person.getUsername(),
                urlAddress,
                person.getActivationCode()
        );
        mailSender.send(person.getMailPerson(), "Activation code", message);
        log.info("Writing user data to the database.\n" + person.toString());
        personRepository.save(person);
        return false;
    }

    @Override
    public boolean activateUser(String code) {
        Person person = personRepository.findByActivationCode(code);

        if (person == null) {
            return false;
        }
        person.setActive(true);
        log.info("Updating user data in the database (Activation).\n" + person.toString());
        person.setActivationCode(null);
        personRepository.save(person);

        return true;
    }

    @Override
    public Person findUserByUsername(String username) {
        Person person = personRepository.findByLoginPerson(username);
        return person;
    }

    @Override
    public boolean addNewUser(Person person, BindingResult bindingResult, Model model, String localAddr) {
        Person userFromDb = findUserByUsername(person.getUsername());
        PasswordEncoder passwordEncoder = springSecurityConfig.getPasswordEncoder();
        if (userFromDb != null) {
            model.addAttribute("exist_error", "Пользователь существует.");
            return true;
        }
        if (bindingResult.hasErrors()) {
            return true;

        } else {
            person.setPasswordPerson(passwordEncoder.encode(person.getPassword()));
            person.setRole(Role.USER);
            person.setActive(false);
            person.setActivationCode(UUID.randomUUID().toString());

            confirmNewUser(person);
        }
        return false;
    }

    @Override
    public boolean updatePassword(Person person, String confPassword, String newPassword) {
        Person oldPerson = personRepository.findById(person.getId());
        if (confPassword != null) {
            if (!newPassword.equals(confPassword))
                return false;
        }
        if (newPassword == null) {
            person.setPasswordPerson(oldPerson.getPassword());
        } else {
            PasswordEncoder passwordEncoder = springSecurityConfig.getPasswordEncoder();
            //String correctLine = escapeHtml4(newPassword);
            //if (correctLine.equals(newPassword))
            person.setPasswordPerson(passwordEncoder.encode(newPassword));
            //else
            //return false;
        }
        person.setActive(oldPerson.isActive());
        person.setRole(oldPerson.getRole());
        personRepository.save(person);
        log.info("Reauthorization");
        Collection<SimpleGrantedAuthority> nowAuthorities = (Collection<SimpleGrantedAuthority>) SecurityContextHolder
                .getContext().getAuthentication().getAuthorities();
        UsernamePasswordAuthenticationToken authentication =
                new UsernamePasswordAuthenticationToken(person.getUsername(), person.getPassword(), nowAuthorities);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return true;
    }

    @Override
    public Person findByLogin(String login) {
        return personRepository.findByLoginPerson(login);
    }

}
