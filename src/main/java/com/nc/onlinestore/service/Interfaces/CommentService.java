package com.nc.onlinestore.service.Interfaces;

import com.nc.onlinestore.model.Category;
import com.nc.onlinestore.model.Comment;
import com.nc.onlinestore.model.Person;
import com.nc.onlinestore.model.Product;

import java.util.List;
import java.util.Optional;

public interface CommentService {
    Optional<Comment> findById(long id);

    List<Comment> findByPerson(Person person);

    List<Comment> findByProduct(Product product);

    Comment save(Comment comment);

    Comment update(Comment comment);

    void delete(long id);
}
