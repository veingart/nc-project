package com.nc.onlinestore.service.Interfaces;

import com.nc.onlinestore.model.Category;

import java.util.List;
import java.util.Optional;

public interface CategoryService {
    List<Category> findAll();

    Optional<Category> findById(long idCategory);

    Category save(Category category);

    Category update(long categoryId, Category category);

    void delete(long id);
}
