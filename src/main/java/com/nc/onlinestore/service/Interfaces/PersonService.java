package com.nc.onlinestore.service.Interfaces;

import com.nc.onlinestore.model.Person;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import java.util.List;

public interface PersonService {
    List<Person> findAll();

    Person findById(long id);

    Person findByMail(String email);

    Person save(Person person);

    Person update(Person person);

    void delete(Long id);

    UserDetails findAuthenticationPerson();

    List<Person> findPersonsForAdmin();

    boolean confirmNewUser(Person person);

    public boolean activateUser(String code);

    Person findUserByUsername(String username);

    boolean addNewUser(Person person, BindingResult bindingResult, Model model, String localAddr);

    boolean updatePassword(Person person, String confPassword, String newPassword);

    Person findByLogin(String login);
}
