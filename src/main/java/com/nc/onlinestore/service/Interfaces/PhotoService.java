package com.nc.onlinestore.service.Interfaces;

import com.nc.onlinestore.model.Photo;
import com.nc.onlinestore.model.PhotoUpload;
import com.nc.onlinestore.model.Product;

import java.io.IOException;
import java.util.List;

public interface PhotoService {
    List<Photo> listPhotos();

    Photo uploadPhoto(PhotoUpload photoUpload, Product product) throws IOException;

    void deletePhoto(Photo photo);

    void save(Photo photo);
}
