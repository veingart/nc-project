package com.nc.onlinestore.service.Interfaces;

import com.nc.onlinestore.enums.Promo;
import com.nc.onlinestore.model.Characteristic;
import com.nc.onlinestore.model.CharacteristicValue;
import com.nc.onlinestore.model.Product;
import com.nc.onlinestore.model.ProductForm;

import java.io.IOException;
import java.util.List;

public interface ProductService {

    List<Product> findAll();

    List<Product> findAllforAdmin();

    List<Product> findTop();

    Product findById(long id);

    Product save(Product product);

    Product findByPromo(Promo promo);

    Product update(long productId, Product product);

    void addNewProduct(ProductForm productForm) throws IOException;

    void delete(Long id);

    void editProduct(ProductForm productForm);

    List<Product> findByCategory(long id);

    List<Product> filter(List<CharacteristicValue> characteristics, long id);

    List<Product> findBycharacteristics(List<Characteristic> characteristics);

    Product setNewRating(Product product);
}
