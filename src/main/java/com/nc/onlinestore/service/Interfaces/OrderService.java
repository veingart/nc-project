package com.nc.onlinestore.service.Interfaces;

import com.nc.onlinestore.enums.Status;
import com.nc.onlinestore.model.Characteristic;
import com.nc.onlinestore.model.Order;
import com.nc.onlinestore.model.OrderArchive;
import com.nc.onlinestore.model.Person;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface OrderService {

    List<Order> findAll();

    Optional<Order> findById(long id);

    Order save(Order order);

    Order update(long orderId, Order order);

    boolean createOneOrderForAll(long productId, int count);

    void delete(Long id);

    List<Order> findByPerson(Person person);

    void changeStatusOnClosed(long id);

    void changeStatusOnExecuted(long id);

    List<Order> findByPersonAndStatus(Person person, Status status);

    List<OrderArchive> findByArchive(Person person);

    double createTotalCost(List<Order> orders);

    void deleteAllByPersonId(long id);

    boolean check();

    List<Order> findAllForAdmin();

    List<Order> findAllForManager(Person person);

    Page<Order> findPaginatedOrder(Pageable pageable, List<Order> orders);
}
