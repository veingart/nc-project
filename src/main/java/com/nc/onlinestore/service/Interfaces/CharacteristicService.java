package com.nc.onlinestore.service.Interfaces;

import com.nc.onlinestore.model.Characteristic;
import com.nc.onlinestore.model.CharacteristicForm;
import com.nc.onlinestore.model.CharacteristicValue;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface CharacteristicService {
    List<Characteristic> findAll();

    List<CharacteristicForm> findAllCharacteristic();

    List<CharacteristicValue> findAllValues();

    Optional<Characteristic> findById(long id);

    Characteristic save(Characteristic characteristic);

    CharacteristicValue findByCharacteristicValue(String characteristicValue);

    CharacteristicValue update(long characteristicId, CharacteristicValue newCharacteristic);

    void delete(long id);

    List<Characteristic> findDistinctByCategoryId(long idCatalog);

    Set<List<Characteristic>> characteristicsCreate(List<Characteristic> characteristics);

    CharacteristicValue findValueById(long id);

    CharacteristicValue saveValue(CharacteristicValue characteristicValue);

    List<CharacteristicValue> saveAllValue(List<CharacteristicValue> characteristicValue);

    List<CharacteristicValue> findAllByCharacteristicCategoryId(long id);

    List<CharacteristicForm> findForFilter(long id);

    ;
}
