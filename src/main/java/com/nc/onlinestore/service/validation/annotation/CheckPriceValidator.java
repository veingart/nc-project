package com.nc.onlinestore.service.validation.annotation;

import com.nc.onlinestore.controller.restController.CharacteristicController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CheckPriceValidator implements ConstraintValidator<CheckPrice, String> {
    private static final Logger log = LoggerFactory.getLogger(CharacteristicController.class);

    @Override
    public boolean isValid(String price, ConstraintValidatorContext constraintValidatorContext) {
       boolean isValid;
        try{
        double priceDouble = Double.parseDouble(price);
        log.info("Checking the field for negative values.");
        isValid = priceDouble > 0;}
        catch (Exception e){
        isValid = false;
    }
        return isValid;
    }
}
