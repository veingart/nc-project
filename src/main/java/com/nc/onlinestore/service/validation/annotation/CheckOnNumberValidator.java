package com.nc.onlinestore.service.validation.annotation;

import com.nc.onlinestore.controller.restController.CharacteristicController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CheckOnNumberValidator implements ConstraintValidator<CheckOnNumber, String> {
    private static final Logger log = LoggerFactory.getLogger(CharacteristicController.class);

    @Override
    public boolean isValid(String name, ConstraintValidatorContext constraintValidatorContext) {
        boolean isValid;
        log.info("Проверка на отсутсвие чисел в поле.");
        isValid = !name.matches(".*[0-9].*");
        return isValid;
    }
}
