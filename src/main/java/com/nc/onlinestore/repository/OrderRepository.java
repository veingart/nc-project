package com.nc.onlinestore.repository;

import com.nc.onlinestore.enums.Status;
import com.nc.onlinestore.model.Order;
import com.nc.onlinestore.model.Person;
import com.nc.onlinestore.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface OrderRepository extends JpaRepository<Order, Long> {
    List<Order> findByPerson(Person person);

    Order findByProductAndPerson(Product product, Person person);

    List<Order> findByPersonAndStatus(Person person, Status status);

    void deleteAllByPersonId(long id);

    Order findByProductId(Long id);

    List<Order> findByStatus(Status status);

    List<Order> findOrderByProduct_PersonAndStatus(Person person, Status status);

}
