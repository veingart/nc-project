package com.nc.onlinestore.repository;

import com.nc.onlinestore.model.Comment;
import com.nc.onlinestore.model.Person;
import com.nc.onlinestore.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CommentRepository extends JpaRepository<Comment, Long> {
    List<Comment> findAllByPerson(Person person);
    List<Comment> findAllByProduct(Product product);
}
