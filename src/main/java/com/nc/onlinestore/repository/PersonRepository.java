package com.nc.onlinestore.repository;

import com.nc.onlinestore.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.core.userdetails.UserDetails;

public interface PersonRepository extends JpaRepository<Person, Long> {
  //  Person findByUsernamePerson(String username);

    Person findById(long id);

    Person findByActivationCode(String code);

    Person findByLoginPerson(String peson);

    Person findByMailPerson(String email);
}
