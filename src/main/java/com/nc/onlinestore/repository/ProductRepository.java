package com.nc.onlinestore.repository;

import com.nc.onlinestore.enums.Promo;
import com.nc.onlinestore.model.Category;
import com.nc.onlinestore.model.Characteristic;
import com.nc.onlinestore.model.CharacteristicValue;
import com.nc.onlinestore.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ProductRepository extends JpaRepository<Product, Long> {
    List<Product> findByCategoryId(long id);
    List<Product> findByCharacteristics(Characteristic characteristic);
    List<Product> findByCharacteristicsIn(List<Characteristic> characteristics);
    List<Product> findByCharacteristicsInAndCategoryId(List<CharacteristicValue> characteristics, long id);
    Product findByPromo(Promo promo);
}
