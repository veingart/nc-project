package com.nc.onlinestore.repository;

import com.nc.onlinestore.model.Order;
import com.nc.onlinestore.model.OrderArchive;
import com.nc.onlinestore.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderArchiveRepository extends JpaRepository<OrderArchive, Long> {
    List<OrderArchive> findByPerson(Person person);
}
