package com.nc.onlinestore.repository;

import com.nc.onlinestore.model.Characteristic;
import com.nc.onlinestore.model.CharacteristicValue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CharacteristicValueRepository extends JpaRepository<CharacteristicValue, Long> {
    CharacteristicValue findByValueCharacteristic(String value);
    List<CharacteristicValue> findAllByCharacteristic(Characteristic characteristic);
    CharacteristicValue findById(long id);
    List<CharacteristicValue> findAllByCharacteristicCategoryId(long id);
}
