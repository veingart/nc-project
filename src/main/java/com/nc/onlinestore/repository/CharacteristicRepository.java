package com.nc.onlinestore.repository;

import com.nc.onlinestore.model.Characteristic;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CharacteristicRepository extends JpaRepository<Characteristic, Long> {

    List<Characteristic> findDistinctByCategoryId(long id);

    List<Characteristic> findAllByCategoryId(long id);
}
