package com.nc.onlinestore.repository;

import com.nc.onlinestore.model.Person;
import com.nc.onlinestore.model.Photo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PhotoRepository extends JpaRepository<Photo, Long> {
    Photo findByTitle(String title);
    Photo findById(long id);


}
