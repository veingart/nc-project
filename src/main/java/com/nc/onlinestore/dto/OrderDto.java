package com.nc.onlinestore.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class OrderDto {
    String date_begin;
    String date_end;
    Date begin;
    Date end;
    Long id;
    String customer;
    String service;
    String executor;
    int order_number;
    float price;
    float duration;
    String serviceId;
    int serviceID;
    String status;
    int executorId;
}
