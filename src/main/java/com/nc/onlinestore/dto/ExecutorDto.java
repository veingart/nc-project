package com.nc.onlinestore.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class ExecutorDto {
    int id;
    String name;
    String surname;
    String services;
    float price;
    float duration;
    int order_id;
    List<Float> durationList = new ArrayList<>();
    List<Float> priceList = new ArrayList<>();
    List<Integer> servicesId = new ArrayList<>();
}
