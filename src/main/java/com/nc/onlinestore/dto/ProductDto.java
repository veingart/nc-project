package com.nc.onlinestore.dto;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.nc.onlinestore.model.Category;
import com.nc.onlinestore.model.Characteristic;
import com.nc.onlinestore.model.Photo;
import com.nc.onlinestore.model.ProductParameter;
import lombok.Data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class ProductDto {
    long id;
    String nameProduct;
    String descriptionProduct;
    double priceProduct;
    int countProduct;
    CategoryDto category;
    List<Characteristic> characteristics;
    PhotoDTO photo;
    Map<String, ProductParameter> params = new HashMap<>();
}
