package com.nc.onlinestore.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class PhotoDTO {
    private Long id;
    private String title;
    private String image;
}
