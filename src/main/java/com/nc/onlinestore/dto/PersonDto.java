package com.nc.onlinestore.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class PersonDto {
    long id;
    String namePerson;
    String surnamePerson;
    String mailPerson;
    String username;
    String password;
    boolean active;
    String activationCode;
}
