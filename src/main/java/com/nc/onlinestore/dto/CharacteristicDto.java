package com.nc.onlinestore.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class CharacteristicDto {
    long id;
    String nameCharacteristic;
    String valueCharacteristic;
    CategoryDto category;
}
