package com.nc.onlinestore.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class ServiceDto {
    int id;
    String service;
    String description;
    String executor;
    float price;
    float duration;
    List<Float> durationList = new ArrayList<>();
    List<Float> priceList = new ArrayList<>();
    List<Integer> executorId = new ArrayList<>();

}
