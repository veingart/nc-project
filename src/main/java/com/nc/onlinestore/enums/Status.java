package com.nc.onlinestore.enums;

public enum Status {
    IN_CART,
    IN_PROCESSING,
    EXECUTED,
    CLOSED,
    ARCHIVED;

    Status() {
    }
}
