package com.nc.onlinestore.model;

import com.nc.onlinestore.service.Impl.Currency;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class AjaxResponse {
    private String msg;
    private List<Product> result;
    private List<Double> currency;
}
