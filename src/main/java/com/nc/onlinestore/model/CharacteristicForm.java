package com.nc.onlinestore.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CharacteristicForm {

    long id;
    String nameCharacteristic;
    List<CharacteristicValue> characteristicValues;
    String nameCategory;

}
