package com.nc.onlinestore.model;

import com.nc.onlinestore.enums.Promo;
import com.nc.onlinestore.service.validation.annotation.CheckOnEmail;
import com.nc.onlinestore.service.validation.annotation.CheckOnNumber;
import com.nc.onlinestore.enums.Role;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import java.util.*;

@Entity
@Table(name = "person")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Person implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @DecimalMin(value = "0.00")
    @Column(name = "person_id")
    private long id;

    @NotBlank(message = "Заполните поле")
    @CheckOnNumber(message = "Уберите цифры из имени")
    @Length(max = 255, message = "Слишком длинное значение")
    @Column(name = "name")
    private String namePerson;

    @NotBlank(message = "Заполните поле")
    @Length(max = 255, message = "Слишком длинное значение")
    @CheckOnNumber(message = "Уберите цифры из фамилии")
    @Column(name = "surname")
    private String surnamePerson;

    @NotBlank(message = "Ошибка")
    @Column(name = "mail")
    @CheckOnEmail
    private String mailPerson;

    @NotBlank(message = "Заполните поле")
    @Length(max = 255, message = "Слишком длинное значение")
    @Column(name = "username")
    private String loginPerson;

    @NotBlank(message = "Заполните поле")
    @Length(max = 255, message = "Слишком длинное значение")
    @Column(name = "password")
    private String passwordPerson;

    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    private Role role;

    @ElementCollection(targetClass = Promo.class, fetch = FetchType.EAGER)
    @CollectionTable(name = "person_promo", joinColumns = @JoinColumn(name = "person_id"))
    @Enumerated(EnumType.STRING)
    private Set<Promo> promos;

    @Column(name = "active")
    private boolean active;

    @Column(name = "activation_code")
    private String activationCode;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Set<Role> roles = new LinkedHashSet<>();
        roles.add(getRole());
        return roles;
    }

    @Override
    public String getPassword() {
        return passwordPerson;
    }

    @Override
    public String getUsername() {
        return loginPerson;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return isActive();
    }

}
