package com.nc.onlinestore.model;

import com.nc.onlinestore.service.validation.annotation.CheckPrice;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductForm {

    @DecimalMin(value = "0.00")
    private long id;

    @NotBlank(message = "Заполните поле")
    @Length(max = 255, message = "Слишком длинное значение")
    private String nameProduct;

    @Length(max = 255, message = "Слишком длинное значение")
    private String descriptionProduct;

    @NotBlank(message = "Заполните поле")
    @CheckPrice(message = "Цена не может быть отрицательной")
    private String priceProduct;

    @DecimalMin(value = "0.00", message = "Значение должно быть 0 или больше нуля.")
    private int countProduct;

    private Person person;

    private PhotoUpload photoUpload;

    private Category category;

    private List<CharacteristicValue> characteristics;

   }
