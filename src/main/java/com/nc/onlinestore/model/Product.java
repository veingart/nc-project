package com.nc.onlinestore.model;

import com.nc.onlinestore.enums.Promo;
import com.nc.onlinestore.enums.Role;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "product")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "product_id")
    private long id;
    @Column(name = "name")
    private String nameProduct;
    @Column(name = "description")
    private String descriptionProduct;
    @Column(name = "price")
    private double priceProduct;
    @Column(name = "count")
    private int countProduct;
    @Column(name = "result_rating")
    double rating;
    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "characteristic_value_id")
    private List<CharacteristicValue> characteristics;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "photoId")
    private Photo photo;
    @Transient
    private String img;
    @ManyToOne
    @JoinColumn(name = "personId")
    private Person person;
    @Column(name = "promo")
    @Enumerated(EnumType.STRING)
    private Promo promo;
}
