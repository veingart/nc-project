package com.nc.onlinestore.model;

import com.cloudinary.StoredFile;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;
import com.cloudinary.*;

@Data
public class PhotoUpload extends StoredFile{
    private String title;

    private MultipartFile file;

    public String getUrl() {
        if (version != null && format != null && publicId != null) {
            return Singleton.getCloudinary().url()
                    .resourceType(resourceType)
                    .type(type)
                    .format(format)
                    .version(version)
                    .generate(publicId);
        } else return null;
    }

    public String getThumbnailUrl() {
        if (version != null && format != null && publicId != null) {
            return Singleton.getCloudinary().url().format(format)
                    .resourceType(resourceType)
                    .type(type)
                    .version(version).transformation(new Transformation().width(150).height(150).crop("fit"))
                    .generate(publicId);
        } else return null;
    }

}
