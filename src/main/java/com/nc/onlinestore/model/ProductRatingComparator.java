package com.nc.onlinestore.model;

import java.util.Comparator;

public class ProductRatingComparator implements Comparator<Product> {
    @Override
    public int compare(Product product, Product product2) {
        if(product.getRating() > product2.getRating()){
            return -1;
        }else if (product.getRating() < product2.getRating()){
            return 1;
        }else return 0;
    }
}
