package com.nc.onlinestore.controller.restController;

import com.nc.onlinestore.dto.CharacteristicDto;
import com.nc.onlinestore.model.Characteristic;
import com.nc.onlinestore.service.Interfaces.CharacteristicService;
import com.nc.onlinestore.service.mapper.CharacteristicMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/characteristic")
//@CrossOrigin(origins = {"http://localhost:4200"})
public class CharacteristicController {

    private static final Logger log = LoggerFactory.getLogger(CharacteristicController.class);

    private final CharacteristicService characteristicService;
    private final CharacteristicMapper characteristicMapper;

    @Autowired
    public CharacteristicController(CharacteristicService characteristicService, CharacteristicMapper characteristicMapper) {
        this.characteristicService = characteristicService;
        this.characteristicMapper = characteristicMapper;
    }

    @GetMapping()
    public List<CharacteristicDto> getAll(Model model) {
        log.info("Get all characteristic {}", characteristicService.findAll());
        return  characteristicService.findAll()
                .stream()
                .filter(Objects::nonNull)
                .map(characteristicMapper::toDto)
                .collect(Collectors.toList());
    }

    @GetMapping("/{characteristicId}")
    public CharacteristicDto getCharacteristic(@PathVariable long characteristicId) {
        log.info("Get characteristic {}", characteristicId);
        return characteristicMapper.toDto(characteristicService.findById(characteristicId).orElse(new Characteristic()));
    }

    @PostMapping()
    public CharacteristicDto createCharacteristic(@RequestBody CharacteristicDto characteristicDto) {
        log.info("Add characteristic {}", characteristicDto);
        Characteristic characteristic = characteristicMapper.toEntity(characteristicDto);
        return characteristicMapper.toDto(characteristicService.save(characteristic));
    }

    @PutMapping("/{characteristicId}")
    public CharacteristicDto updateCharacteristic(@PathVariable long characteristicId,
                                      @RequestBody CharacteristicDto characteristicDto) {
        Characteristic characteristic = characteristicMapper.toEntity(characteristicDto);
        log.info("Update characteristic {} ", characteristicId);
        return null;
    }

    @DeleteMapping("/{characteristicId}")
    public ResponseEntity<?> deleteCharacteristic(@PathVariable long characteristicId) {
        log.info("Delete characteristic {}", characteristicId);
        characteristicService.delete(characteristicId);
        return ResponseEntity.ok().build();
    }
}
