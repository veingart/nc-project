package com.nc.onlinestore.controller.restController;

import com.nc.onlinestore.dto.CategoryDto;
import com.nc.onlinestore.model.Category;
import com.nc.onlinestore.service.Interfaces.CategoryService;
import com.nc.onlinestore.service.mapper.CategoryMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/category")
//@CrossOrigin(origins = {"http://localhost:4200"})
public class CategoryController {

    private static final Logger log = LoggerFactory.getLogger(CategoryController.class);

    private final CategoryService categoryService;
    private final CategoryMapper categoryMapper;

    @Autowired
    public CategoryController(CategoryService categoryService, CategoryMapper categoryMapper) {
        this.categoryService = categoryService;
        this.categoryMapper = categoryMapper;
    }


    @GetMapping()
    public List<CategoryDto> getAll(Model model) {
        log.info("Get all categories {}", categoryService.findAll());
        return  categoryService.findAll()
                .stream()
                .filter(Objects::nonNull)
                .map(categoryMapper::toDto)
                .collect(Collectors.toList());
    }

    @GetMapping("/{categoryId}")
    public CategoryDto getCategory(@PathVariable long categoryId) {
        log.info("Get category {}", categoryId);
        return categoryMapper.toDto(categoryService.findById(categoryId).orElse(new Category()));
    }

    @PostMapping()
    public CategoryDto createCategory(@RequestBody CategoryDto categoryDto) {
        Category category = categoryMapper.toEntity(categoryDto);
        log.info("Add category {}", categoryDto);
        return categoryMapper.toDto(categoryService.save(category));
    }

    @PutMapping("/{categoryId}")
    public CategoryDto updateCategory(@PathVariable long categoryId,
                                      @RequestBody CategoryDto categoryDto) {
        Category category = categoryMapper.toEntity(categoryDto);
        log.info("Update category {} ", categoryId);
        return categoryMapper.toDto(categoryService.update(categoryId, category));
    }

    @DeleteMapping("/{categoryId}")
    public ResponseEntity<?> deleteCategory(@PathVariable long categoryId) {
        log.info("Delete category {}", categoryId);
        categoryService.delete(categoryId);
        return ResponseEntity.ok().build();
    }
}

