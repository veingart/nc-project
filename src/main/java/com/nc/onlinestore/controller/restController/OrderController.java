package com.nc.onlinestore.controller.restController;

import com.nc.onlinestore.dto.OrderDto;
import com.nc.onlinestore.dto.ResponseDto;
import com.nc.onlinestore.enums.Status;
import com.nc.onlinestore.model.CharacteristicValue;
import com.nc.onlinestore.model.Order;
import com.nc.onlinestore.service.Interfaces.OrderService;
import com.nc.onlinestore.service.mapper.OrderMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/order")
public class OrderController {

    private static final Logger log = LoggerFactory.getLogger(OrderController.class);

    private final OrderService orderService;
    private final OrderMapper orderMapper;

    @Autowired
    public OrderController(OrderService orderService, OrderMapper orderMapper) {
        this.orderService = orderService;
        this.orderMapper = orderMapper;
    }


    @PostMapping()
    public void createOrder(@RequestBody ResponseDto responseDto) {
        Long id = Long.parseLong(responseDto.getItemId());
        orderService.changeStatusOnExecuted(id);
    }


}
