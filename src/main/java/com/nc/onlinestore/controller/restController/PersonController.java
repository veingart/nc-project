package com.nc.onlinestore.controller.restController;

import com.nc.onlinestore.dto.PersonDto;
import com.nc.onlinestore.enums.Promo;
import com.nc.onlinestore.model.Person;
import com.nc.onlinestore.service.Interfaces.PersonService;
import com.nc.onlinestore.service.mapper.PersonMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/person")
public class PersonController {

    private static final Logger log = LoggerFactory.getLogger(PersonController.class);

    private final PersonService personService;
    private final PersonMapper personMapper;

    @Autowired
    public PersonController(PersonService personService, PersonMapper personMapper) {
        this.personService = personService;
        this.personMapper = personMapper;
    }

    @GetMapping()
    public List<PersonDto> getAll(Model model) {
        log.info("Get all users {}", personService.findAll());
        return  personService.findAll()
                .stream()
                .filter(Objects::nonNull)
                .map(personMapper::toDto)
                .collect(Collectors.toList());
    }

    @GetMapping("/{email}/promocodes")
    public Set<Promo> getPromoByPerson(@PathVariable String email) {
        log.info("Get user {}", email);
        Person person = personService.findByMail(email);
        if (person == null) {
            return null;
        } else return person.getPromos();
    }

    @PostMapping()
    public PersonDto createPerson(@RequestBody PersonDto personDto) {
        Person person = personMapper.toEntity(personDto);
        log.info("Add user {}", personDto);
        return personMapper.toDto(personService.save(person));
    }

    @PutMapping("/{personId}")
    public PersonDto updatePerson(@PathVariable long personId,
                                @RequestBody PersonDto personDto) {
        Person person = personMapper.toEntity(personDto);
        log.info("Update user {} ", personId);
        return personMapper.toDto(personService.update(person));
    }

    @DeleteMapping("/{personId}")
    public ResponseEntity<?> deletePerson(@PathVariable long personId) {
        log.info("Delete user {}", personId);
        personService.delete(personId);
        return ResponseEntity.ok().build();
    }
}
