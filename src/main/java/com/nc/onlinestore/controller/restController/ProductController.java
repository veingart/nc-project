package com.nc.onlinestore.controller.restController;

import com.nc.onlinestore.dto.ProductDto;
import com.nc.onlinestore.model.Product;
import com.nc.onlinestore.service.Interfaces.ProductService;
import com.nc.onlinestore.service.mapper.ProductMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/products")
//@CrossOrigin(origins = {"http://localhost:4200"})
public class ProductController {

    private static final Logger log = LoggerFactory.getLogger(ProductController.class);


    private final ProductService productService;
    private final ProductMapper productMapper;

    @Autowired
    public ProductController(ProductService productService, ProductMapper productMapper) {
        this.productService = productService;
        this.productMapper = productMapper;
    }


    @GetMapping()
    public List<ProductDto> getAll(Model model) {
        return productService.findAll()
                .stream()
                .filter(Objects::nonNull)
                .map(productMapper::toDto)
                .collect(Collectors.toList());
    }

    @GetMapping("/{categoryId}")
    public ProductDto getProduct(@PathVariable long categoryId) {
        return productMapper.toDto(productService.findById(categoryId));
    }

    @PostMapping()
    public ProductDto createCategory(@RequestBody ProductDto productDto) {
        Product product = productMapper.toEntity(productDto);
        return productMapper.toDto(productService.save(product));
    }

    @PutMapping("/{productId}")
    public ProductDto updateCategory(@PathVariable long productId,
                                     @RequestBody ProductDto productDto) {
        Product product = productMapper.toEntity(productDto);
        return productMapper.toDto(productService.update(productId, product));
    }

    @DeleteMapping("/{productId}")
    public ResponseEntity<?> deleteCategory(@PathVariable long productId) {
        productService.delete(productId);
        return ResponseEntity.ok().build();
    }
}
