package com.nc.onlinestore.controller.baseController;

import com.nc.onlinestore.model.Category;
import com.nc.onlinestore.model.Person;
import com.nc.onlinestore.service.Interfaces.CategoryService;
import com.nc.onlinestore.service.Interfaces.PersonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Controller
public class RegistrationController {
    private static final Logger log = LoggerFactory.getLogger(RegistrationController.class);

    private final PersonService personService;
    private final CategoryService categoryService;

    @Autowired
    public RegistrationController(PersonService personService, CategoryService categoryService) {
        this.personService = personService;
        this.categoryService = categoryService;
    }


    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registration(Model model) {
        log.info("Go to the registration page.");
        List<Category> categories = categoryService.findAll();
        model.addAttribute("categories", categories);
        model.addAttribute("personForm", new Person());
        return "registration";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registration(@ModelAttribute("personForm") @Valid Person person, BindingResult bindingResult, Model model, HttpServletRequest request) {

        if (personService.addNewUser(person, bindingResult, model, request.getLocalAddr())) return "registration";
        log.info("New User Registration.");
        return "redirect:/login";

    }

    @GetMapping("/activate/{code}")
    public String activate(Model model, @PathVariable String code) {
        boolean isActivated = personService.activateUser(code);
        List<Category> categories = categoryService.findAll();
        model.addAttribute("categories", categories);
        if (isActivated) {
            model.addAttribute("message", "Ваша активация прошла успешно");
            log.info("New user activation.");
        } else {
            model.addAttribute("message", "Код активации не найден!");
            log.info("Activation code not found!(" + code + ")");
        }
        model.addAttribute("isActivated", isActivated);
        return "login";
    }

}
