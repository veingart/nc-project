package com.nc.onlinestore.controller.baseController;

import com.nc.onlinestore.model.Category;
import com.nc.onlinestore.model.Person;
import com.nc.onlinestore.service.Impl.CategoryServiceImpl;
import com.nc.onlinestore.service.Impl.PersonServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@Controller
@PreAuthorize("hasAnyAuthority('ADMIN','USER','MANAGER')")
public class ProfileController {
    private static final Logger log = LoggerFactory.getLogger(ProfileController.class);

    private final CategoryServiceImpl categoryService;
    private final PersonServiceImpl personService;

    @Autowired
    public ProfileController(CategoryServiceImpl categoryService, PersonServiceImpl personService) {
        this.categoryService = categoryService;
        this.personService = personService;
    }


    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public String profile(Model model) {
        log.info("Go to profile page.");
        List<Category> categories = categoryService.findAll();
        Person person = (Person) personService.findAuthenticationPerson();
        model.addAttribute("categories", categories);
        model.addAttribute("person", person);
        model.addAttribute("personForm", new Person());
        return "profile";
    }

    @RequestMapping(value = "/profile/update", method = RequestMethod.POST)
    public String updatePerson(@ModelAttribute("personForm") @Valid Person person,
                               BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            Map<String, String> errorMap = ControllerUtils.getErrors(bindingResult);
            log.info("Errors occurred in input fields when updating user\n" + errorMap);
            model.mergeAttributes(errorMap);
            model.addAttribute("person", person);
            return "redirect:/profile";
        } else {
            log.info("Update user.");
            personService.update(person);
            return "redirect:/profile";
        }
    }

    @RequestMapping(value = "/profile/update-password", method = RequestMethod.POST)
    public String updatePerson(@Param("newPassword") @Valid String newPassword,
                               @Param("confPassword") @Valid String confPassword, Model model) {
        if (newPassword != null && newPassword.length() <= 255
                && confPassword != null && confPassword.length() <= 255) {
            Person person = (Person) personService.findAuthenticationPerson();
            boolean isValid = personService.updatePassword(person, confPassword, newPassword);
            if (!isValid) {
                log.info("Errors occurred in input fields when updating user.");
                model.addAttribute("incorrect_password", "Не получилось изменить.");
                return "error";
            }
            log.info("Update user.");
            return "redirect:/profile";
        }
        log.info("Errors occurred in input fields when updating user");
        model.addAttribute("incorrect_password", "Не корректно введены данные для изменения пароля.");
        return "error";
    }
}
