package com.nc.onlinestore.controller.baseController;


import com.nc.onlinestore.model.Category;
import com.nc.onlinestore.model.Product;
import com.nc.onlinestore.service.Impl.CategoryServiceImpl;
import com.nc.onlinestore.service.Impl.Currency;
import com.nc.onlinestore.service.Impl.OrderServiceImpl;
import com.nc.onlinestore.service.Impl.ProductServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;


@Controller
public class MainController {
    private static final Logger log = LoggerFactory.getLogger(MainController.class);

    private final CategoryServiceImpl categoryService;
    private final ProductServiceImpl productService;
    private final OrderServiceImpl orderService;

    @Autowired
    public MainController(CategoryServiceImpl categoryService, ProductServiceImpl productService, OrderServiceImpl orderService) {
        this.categoryService = categoryService;
        this.productService = productService;
        this.orderService = orderService;
    }

    @RequestMapping("/")
    public String home(Model model) {
        log.info("Go to the home page.");
        List<Product> products = productService.findTop();
        List<Category> categories = categoryService.findAll();
        model.addAttribute("categories", categories);
        model.addAttribute("products", products);
        model.addAttribute("currency", Currency.getDollarCurrency());
        return "home";
    }
    @PreAuthorize("hasAnyAuthority('ADMIN','USER','MANAGER')")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String createOneOrder(@RequestParam(name = "productId") long idProduct, @Param("count") int count, Model model) {

        boolean isOrderCreate = orderService.createOneOrderForAll(idProduct, count);
        if (!isOrderCreate) {
            log.info("An error occurred while adding the item to the cart.");
            model.addAttribute("message_end", "УПС! У нас нет столько товаров сколько Вы добавляете в корзину.");
            return "error";
        } else {
            log.info("Adding item to cart.");
            return "redirect:/";
        }
    }

    @RequestMapping("/login")
    public String login(Model model) {
        log.info("Go to the login page.");
        List<Category> categories = categoryService.findAll();
        model.addAttribute("categories", categories);
        return "login";
    }
}
