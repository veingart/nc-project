package com.nc.onlinestore.controller.baseController;

import com.nc.onlinestore.dto.ExecutorDto;
import com.nc.onlinestore.dto.ServiceDto;
import com.nc.onlinestore.enums.Promo;
import com.nc.onlinestore.enums.Status;
import com.nc.onlinestore.model.*;
import com.nc.onlinestore.service.Impl.*;
import com.nc.onlinestore.service.Interfaces.OrderService;
import org.aspectj.weaver.ast.Or;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.thymeleaf.util.ListUtils;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Controller
@PreAuthorize("hasAnyAuthority('ADMIN','USER','MANAGER')")
public class CartController {
    private static final Logger log = LoggerFactory.getLogger(CatalogController.class);

    private final CategoryServiceImpl categoryService;
    private final PersonServiceImpl personService;
    private final OrderServiceImpl orderService;
    private final ProductServiceImpl productService;

    @Autowired
    public CartController(CategoryServiceImpl categoryService, PersonServiceImpl personService, OrderServiceImpl orderService, ProductServiceImpl productService) {
        this.categoryService = categoryService;
        this.personService = personService;
        this.orderService = orderService;
        this.productService = productService;
    }

    @RequestMapping(value = "/cart", method = RequestMethod.GET)
    public String cart(Model model) {
        log.info("Go to user’s cart page");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Person person = personService.findByLogin(auth.getName());
        List<Category> categories = categoryService.findAll();
        List<Order> orders = orderService.findByPersonAndStatus(person, Status.IN_CART);
        double totalPrice = orderService.createTotalCost(orders);
        HashSet promos = new HashSet<>();
        try {
            promos = Integration.getPromo(person.getMailPerson());
            if (promos.contains("FREEMP3") && person.getPromos().contains(Promo.FREEMP3) == false) {
                Set<Promo> promoSet = person.getPromos();
                promoSet.add(Promo.FREEMP3);
                person.setPromos(promoSet);
                Order order = new Order();
                order.setPerson(person);
                Product product = productService.findByPromo(Promo.FREEMP3);
                product.setPriceProduct(0);
                order.setProduct(product);
                order.setCountOrder(1);
                order.setStatus(Status.IN_CART);
                orderService.save(order);
            }
        } catch (Exception e) {

        }
        if (totalPrice >= 500 & person.getPromos().contains("FREESERVICE") == false){
            Set<Promo> promo = person.getPromos();
            promo.add(Promo.FREESERVICE);
            personService.save(person);
        }

        model.addAttribute("currency", Currency.getDollarCurrency());
        model.addAttribute("totalPrice", totalPrice);
        model.addAttribute("categories", categories);
        model.addAttribute("orders", orders);
        return "cart";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public String deleteOrder(@RequestParam(name = "orderId") long idOrder) {
        log.info("Delete order");

        Order order = orderService.findById(idOrder).orElse(new Order());
        if (order.getProduct().getId() == 2) {
            order.getPerson().getPromos().remove(Promo.FREEMP3);
            orderService.delete(idOrder);
        } else {
            orderService.delete(idOrder);
        }
        return "redirect:cart";
    }

    @RequestMapping(value = "/updateOrder", method = RequestMethod.GET)
    public String updateOrder(@RequestParam(name = "orderId") long idOrder, Model model) {
        log.info("Upd order");
        Order order = orderService.findById(idOrder).orElse(new Order());
        model.addAttribute("order", order);
        return "edit_order";
    }

    @RequestMapping(value = "/updateOrder", method = RequestMethod.POST)
    public String updateOrder(@ModelAttribute("order") @Valid Order order, BindingResult bindingResult, Model model) {
        log.info("Upd order");
        long id = order.getId();
        orderService.update(id, order);
        return "redirect:cart";
    }

    @RequestMapping(value = "/checkout", method = RequestMethod.GET)
    public String checkoutOrders(Model model) {
        if (!orderService.check()) {
            model.addAttribute("message_busy", "Один или несколько из товаров не были оформлены. " +
                    "Причина: отсутсвие товара в наличии, возможно его заказали первее Вас.");
            return "error";
        }
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Person person = personService.findByLogin(auth.getName());
        if (person.getPromos().contains(Promo.FREESERVICE)==true){
            List<ServiceDto> services = Integration.getServices();
            List<ExecutorDto> executors = Integration.getExecutors();
            model.addAttribute("services", services);
            model.addAttribute("executors", executors);
            return "service";
        }
        return "redirect:cart";
    }

    @RequestMapping(value = "/orders", method = RequestMethod.GET)
    public String myOrders(Model model) {
        log.info("Go to user’s cart page");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Person person = personService.findByLogin(auth.getName());
        List<Category> categories = categoryService.findAll();
        List<Order> orders = orderService.findByPersonAndStatus(person, Status.IN_PROCESSING);
        List<Order> exec = orderService.findByPersonAndStatus(person, Status.EXECUTED);
        List<OrderArchive> closed = orderService.findByArchive(person);
        double totalPrice = orderService.createTotalCost(orders);
        for (int i = 0; i < orders.size(); i++) {
            Order order = orders.get(i);
            String url = "https://bonkbank.gq/services/payments/elmart?cost=" +
                    order.getProduct().getPriceProduct() * order.getCountOrder() + "&itemId=" + order.getId()+"&currency=BYN";
            order.setUrl(url);
        }
        orders.addAll(exec);
        model.addAttribute("currency", Currency.getDollarCurrency());
        model.addAttribute("totalPrice", totalPrice);
        model.addAttribute("categories", categories);
        model.addAttribute("orders", orders);
        model.addAttribute("closed", closed);
        return "orders";
    }

}
