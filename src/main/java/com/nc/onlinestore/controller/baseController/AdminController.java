package com.nc.onlinestore.controller.baseController;


import com.cloudinary.Singleton;
import com.cloudinary.utils.ObjectUtils;
import com.nc.onlinestore.model.*;
import com.nc.onlinestore.service.Interfaces.*;
import org.aspectj.weaver.ast.Or;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@PreAuthorize("hasAnyAuthority('ADMIN','MANAGER')")
public class AdminController {
    private static final Logger log = LoggerFactory.getLogger(CatalogController.class);

    private final ProductService productService;
    private final PersonService personService;
    private final OrderService orderService;
    private final CharacteristicService characteristicService;
    private final CategoryService categoryService;
    private final PhotoService photoService;

    @Autowired
    public AdminController(ProductService productService, PersonService personService, OrderService orderService, CharacteristicService characteristicService, CategoryService categoryService, PhotoService photoService) {
        this.productService = productService;
        this.personService = personService;
        this.orderService = orderService;
        this.characteristicService = characteristicService;
        this.categoryService = categoryService;
        this.photoService = photoService;
    }


    @RequestMapping("/admin")
    public String admin(Model model) {
        log.info("Go to admin panel.");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Person person = personService.findByLogin(auth.getName());
        List<Category> categories = categoryService.findAll();
        List<Person> people = personService.findPersonsForAdmin();
        List<Product> products = productService.findAllforAdmin();
        List<Order> orders;
        if (person.getRole().equals("ADMIN")) {
         orders = orderService.findAllForAdmin();
        }else {
         orders = orderService.findAllForManager(person);
        }
        Characteristic characteristic = new Characteristic();
        model.addAttribute("categories", categories);
        model.addAttribute("characts", characteristicService.findAllValues());
        model.addAttribute("productForm", new ProductForm());
        model.addAttribute("categoryForm", new Category());
        model.addAttribute("characteristicForm", characteristic);
        model.addAttribute("people", people);
        model.addAttribute("products", products);
        model.addAttribute("orders", orders);

        return "admin";
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/update", method = RequestMethod.GET)
    public String updatePerson(@RequestParam(name = "personId") long idPerson, Model model) {
        log.info("Go to the user data update page");
        List<Category> categories = categoryService.findAll();

        model.addAttribute("categories", categories);
        Person person = personService.findById(idPerson);
        model.addAttribute("personForm", new Person());
        model.addAttribute("person", person);
        return "registration";
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String updatePerson(@ModelAttribute("personForm") @Valid Person person, BindingResult bindingResult, Model model) {

        if (bindingResult.hasErrors()) {
            Map<String, String> errorMap = ControllerUtils.getErrors(bindingResult);
            model.mergeAttributes(errorMap);
            log.info("Errors occurred in input fields when updating user\n" + errorMap);
            model.addAttribute("person", person);
            return "registration";

        } else {
            Person oldPerson = personService.findById(person.getId());
            person.setPasswordPerson(oldPerson.getPassword());
            person.setActive(oldPerson.isActive());
            log.info("Update user.");
            personService.save(person);
            return "redirect:/admin";
        }
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/delete-person", method = RequestMethod.POST)
    public String deletePerson(@RequestParam(name = "personId") long idPerson) {
        log.info("Delete user");
        personService.delete(idPerson);
        return "redirect:/admin";
    }

    @RequestMapping(value = "/edit-product", method = RequestMethod.GET)
    public String editProduct(@RequestParam(name = "productId") long idProduct, Model model) {
        log.info("Go to the component data update page");
        List<Category> categories = categoryService.findAll();
        model.addAttribute("categories", categories);

        Product product = productService.findById(idProduct);
        model.addAttribute("productForm", new ProductForm());
        model.addAttribute("characts", characteristicService.findAll());
        model.addAttribute("product", product);
        return "edit_product";
    }

    @RequestMapping(value = "/edit-product", method = RequestMethod.POST)
    public String editExistProduct(@ModelAttribute("productForm") @Valid ProductForm productForm, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            List<Category> categories = categoryService.findAll();
            model.addAttribute("categories", categories);

            Map<String, String> errorMap = ControllerUtils.getErrors(bindingResult);
            log.info("Errors occurred in input fields when updating component\n" + errorMap);
            model.mergeAttributes(errorMap);
            model.addAttribute("product", productForm);
            return "edit_product";

        } else {
            log.info("Update product");
            productService.editProduct(productForm);
            return "redirect:/admin";
        }

    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public String uploadPhoto(@ModelAttribute PhotoUpload photoUpload, BindingResult result, ModelMap model) throws IOException, IOException {

        Map uploadResult = null;
        if (photoUpload.getFile() != null && !photoUpload.getFile().isEmpty()) {
            uploadResult = Singleton.getCloudinary().uploader().upload(photoUpload.getFile().getBytes(),
                    ObjectUtils.asMap("resource_type", "auto"));
            photoUpload.setPublicId((String) uploadResult.get("public_id"));
            Object version = uploadResult.get("version");
            if (version instanceof Integer) {
                photoUpload.setVersion(new Long((Integer) version));
            } else {
                photoUpload.setVersion((Long) version);
            }

            photoUpload.setSignature((String) uploadResult.get("signature"));
            photoUpload.setFormat((String) uploadResult.get("format"));
            photoUpload.setResourceType((String) uploadResult.get("resource_type"));
        }

        if (result.hasErrors()) {
            model.addAttribute("photoUpload", photoUpload);
            return "redirect:/admin";
        } else {
            Photo photo = new Photo();
            photo.setUpload(photoUpload);
            model.addAttribute("upload", uploadResult);
            photoService.save(photo);
            model.addAttribute("photo", photo);
            return "redirect:/admin";
        }
    }


    @RequestMapping(value = "/add-product", method = RequestMethod.POST)
    public String createNewProduct(@ModelAttribute("productForm") @Valid ProductForm productForm, BindingResult bindingResult, Model model) throws IOException {
        if (bindingResult.hasErrors()) {
            List<Category> categories = categoryService.findAll();
            model.addAttribute("categories", categories);

            Map<String, String> errorMap = ControllerUtils.getErrors(bindingResult);
            log.info("Errors occurred in input fields when adding component\n" + errorMap);
            model.mergeAttributes(errorMap);
            return "redirect:/admin";

        } else {
            log.info("Add new product");
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            Person person = personService.findByLogin(auth.getName());
            productForm.setPerson(person);
            productService.addNewProduct(productForm);
            return "redirect:/admin";
        }
    }

    @RequestMapping(value = "/delete-product", method = RequestMethod.POST)
    public String deleteProduct(@RequestParam(name = "productId") long idProduct) {
        log.info("Delete product");
        productService.delete(idProduct);
        return "redirect:/admin";
    }

    @RequestMapping(value = "/add-category", method = RequestMethod.POST)
    public String createNewCategory(@ModelAttribute("categoryForm") @Valid Category category, BindingResult bindingResult, Model model) {

        if (bindingResult.hasErrors()) {
            Map<String, String> errorMap = ControllerUtils.getErrors(bindingResult);
            log.info("Errors occurred in input fields when adding category\n" + errorMap);
            model.mergeAttributes(errorMap);
            model.addAttribute("category", category);
            return "redirect:/admin";
        } else {
            log.info("Add new category");
            categoryService.save(category);
            return "redirect:/admin";
        }

    }

    @RequestMapping(value = "/edit-category", method = RequestMethod.GET)
    public String editCategory(@RequestParam(name = "categoryId") long idCategory, Model model) {
        List<Category> categories = categoryService.findAll();
        model.addAttribute("categories", categories);
        log.info("Go to the category data update page");
        Category category = categoryService.findById(idCategory).get();
        model.addAttribute("category", category);
        model.addAttribute("categoryForm", new Category());
        return "edit_charact_or_category";
    }

    @RequestMapping(value = "/edit-category", method = RequestMethod.POST)
    public String editExistCategory(@ModelAttribute("categoryForm") @Valid Category category, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            List<Category> categories = categoryService.findAll();
            model.addAttribute("categories", categories);

            Map<String, String> errorMap = ControllerUtils.getErrors(bindingResult);
            log.info("Errors occurred in input fields when updating category\n" + errorMap);
            model.mergeAttributes(errorMap);
            model.addAttribute("category", category);
            return "edit_charact_or_category";
        } else {
            log.info("Update category");
            categoryService.update(category.getId(), category);
            return "redirect:/admin";
        }

    }

    @RequestMapping(value = "/delete-category", method = RequestMethod.POST)
    public String deleteCategory(@RequestParam(name = "categoryId") long idCategory) {
        log.info("Delete category");
        categoryService.delete(idCategory);
        return "redirect:/admin";
    }

    @RequestMapping(value = "/add-characteristic", method = RequestMethod.POST)
    public String addNewCharacteristic(@ModelAttribute("characteristicForm") @Valid Characteristic characteristic,
                                       BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            Map<String, String> errorMap = ControllerUtils.getErrors(bindingResult);
            model.mergeAttributes(errorMap);
            log.info("Errors occurred in input fields when adding characteristic\n" + errorMap);
            model.addAttribute("characteristic", characteristic);
            return "redirect:/admin";
        } else {
            log.info("Add new characteristic");
            characteristicService.save(characteristic);
            List<CharacteristicValue> values = characteristic.getCharacteristicValues();
            for (int i = 0; i<values.size(); i++){
                CharacteristicValue characteristicValue = new CharacteristicValue();
                characteristicValue.setValueCharacteristic(values.get(i).getValueCharacteristic());
                characteristicValue.setCharacteristic(characteristic);
                characteristicService.saveValue(characteristicValue);
            }

            return "redirect:/admin";
        }
    }

    @RequestMapping(value = "/edit-characteristic", method = RequestMethod.GET)
    public String editCharacteristic(@RequestParam(name = "characteristicId") long idCharacteristic, Model model) {
        List<Category> categories = categoryService.findAll();
        model.addAttribute("categories", categories);
        log.info("Go to the characteristic data update page");
        CharacteristicValue characteristicValue = characteristicService.findValueById(idCharacteristic);
        model.addAttribute("characteristicValue", characteristicValue);
        model.addAttribute("characteristicForm", new CharacteristicValue());
        return "edit_charact_or_category";
    }

    @RequestMapping(value = "/edit-characteristic", method = RequestMethod.POST)
    public String editExistCharacteristic(@ModelAttribute("characteristicForm") @Valid CharacteristicValue characteristicValue, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            List<Category> categories = categoryService.findAll();
            model.addAttribute("categories", categories);

            Map<String, String> errorMap = ControllerUtils.getErrors(bindingResult);
            log.info("Errors occurred in input fields when updating characteristic\n" + errorMap);
            model.mergeAttributes(errorMap);
            model.addAttribute("characteristic", characteristicValue);
            return "edit_charact_or_category";
        } else {
            log.info("Update characteristic");
            characteristicService.update(characteristicValue.getId(), characteristicValue);
            return "redirect:/admin";
        }
    }

    @RequestMapping(value = "/delete-characteristic", method = RequestMethod.POST)
    public String deleteCharacteristic(@RequestParam(name = "characteristicId") long idCharacteristic) {
        log.info("Delete characteristic");

        characteristicService.delete(idCharacteristic);
        return "redirect:/admin";
    }

    @RequestMapping(value = "/delete-order", method = RequestMethod.POST)
    public String deleteOrderAdmin(@RequestParam(name = "orderId") long idOrder, Model model) {
        log.info("Delete order");
        orderService.delete(idOrder);
        return "redirect:/admin";
    }

    @RequestMapping(value = "/executed", method = RequestMethod.POST)
    public String editStatusOnExecuted(@RequestParam(name = "orderId") long idOrder) {
        orderService.changeStatusOnExecuted(idOrder);
        log.info("Update status order");
        return "redirect:/admin";
    }

    @RequestMapping(value = "/closed", method = RequestMethod.POST)
    public String editStatusOnClosed(@RequestParam(name = "orderId") long idOrder) {
        orderService.changeStatusOnClosed(idOrder);
        log.info("Update status order");
        return "redirect:/admin";
    }
}
