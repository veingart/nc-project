package com.nc.onlinestore.controller.baseController;

import com.nc.onlinestore.model.*;
import com.nc.onlinestore.repository.CommentRepository;
import com.nc.onlinestore.service.Impl.*;
import com.nc.onlinestore.service.Impl.Currency;
import com.nc.onlinestore.service.Interfaces.CommentService;
import com.nc.onlinestore.service.Interfaces.PersonService;
import com.nc.onlinestore.service.mapper.ProductMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;


@Controller
public class CatalogController {

    private static final Logger log = LoggerFactory.getLogger(CatalogController.class);

    private final CategoryServiceImpl categoryService;
    private final ProductServiceImpl productService;
    private final CharacteristicServiceImpl characteristicService;
    private final OrderServiceImpl orderService;
    private final ProductMapper productMapper;
    private final PersonService personService;
    private final CommentService commentService;

    private long catalog_id;

    @Autowired
    public CatalogController(CategoryServiceImpl categoryService, ProductServiceImpl productService, CharacteristicServiceImpl characteristicService, OrderServiceImpl orderService, ProductMapper productMapper, PersonService personService, CommentService commentService) {
        this.categoryService = categoryService;
        this.productService = productService;
        this.characteristicService = characteristicService;
        this.orderService = orderService;
        this.productMapper = productMapper;
        this.personService = personService;
        this.commentService = commentService;
    }


    @RequestMapping(value = "/catalog", method = RequestMethod.GET)
    public String oneCategory(@RequestParam(name = "categoryId") long idCatalog, Model model) {
        log.info("Go to catalog page");
        Category category = null;
        catalog_id = idCatalog;
        List<Category> categories = categoryService.findAll();
        List<Product> products;
        if (idCatalog != 0) {
            List<CharacteristicForm> characteristics = characteristicService.findForFilter(idCatalog);
            model.addAttribute("characts", characteristics);
        } else {
            model.addAttribute("characts", null);
        }


        if (idCatalog != 0)
            category = categoryService.findById(idCatalog).orElse(new Category());
        model.addAttribute("categories", categories);
        model.addAttribute("category", category);

        return "catalog";
    }

    @RequestMapping(value = "/catalog/good", method = RequestMethod.GET)
    public String product(@RequestParam(name = "productId") long productId, Model model) {
        log.info("Go to product page");
        List<Category> categories = categoryService.findAll();
        Product product = productService.findById(productId);
        List<Comment> comments = commentService.findByProduct(product);
        productService.setNewRating(product);
        model.addAttribute("categories", categories);
        model.addAttribute("product", product);
        Comment comment = new Comment();
        comment.setProduct(product);
        model.addAttribute("comments", comments);
        model.addAttribute("newComment",comment);
        return "good";
    }
    @PreAuthorize("hasAnyAuthority('ADMIN','MANAGER', 'USER')")
    @RequestMapping(value = "/addComment", method = RequestMethod.POST)
    public String comment(@ModelAttribute("comment") @Valid Comment comment, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Person person = personService.findByLogin(auth.getName());
        log.info("Add comment");
        comment.setPerson(person);
        comment.setDate(new Date());
        commentService.save(comment);
        productService.setNewRating(comment.getProduct());
        return "redirect:/";
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','USER','MANAGER')")
    @RequestMapping(value = "/catalog/add", method = RequestMethod.POST)
    public String createOrderForCatalog(@RequestParam(name = "productId") long productId, @Param("count") int count, Model model) {
        boolean isOrderCreate = orderService.createOneOrderForAll(productId, count);
        if (!isOrderCreate) {
            model.addAttribute("message_end", "УПС! У нас нет столько товаров сколько Вы добавляете в корзину.");
            log.info("An error occurred while adding the item.");
            return "error";
        } else {
            log.info("Adding item to cart");
            return "redirect:/catalog?categoryId=" + catalog_id;
        }

    }

    @PostMapping("/filter")
    public ResponseEntity<?> getFilterResultViaAjax(@Valid @RequestBody String characteristic_id, Errors errors) {

        AjaxResponse result = new AjaxResponse();
        if (errors.hasErrors()) {
            result.setMsg(errors.getAllErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.joining(",")));
            return ResponseEntity.badRequest().body(result);
        }
        List<Product> products = new ArrayList<>();
        if (characteristic_id.isEmpty()){
            characteristic_id = "0";
        }
        characteristic_id = characteristic_id.replaceAll("[^0-9]+", " ");
        List<String> characteristicList = Arrays.asList(characteristic_id.trim().split(" "));

        if (characteristic_id.matches(".*[0-9].*")) {
            List<CharacteristicValue> characteristics = new ArrayList<>();

            for (int i = 0; i < characteristicList.size(); i++) {
                Long id = Long.parseLong(characteristicList.get(i));
                characteristics.add(characteristicService.findValueById(id));
            }
            if (catalog_id != 0) {
                products = productService.filter(characteristics, catalog_id);
                for (int i = 0; i < products.size(); i++) {
                    Product product = products.get(i);
                    product.setImg(product.getPhoto().getImage());
                }
                result.setResult(products);
            }

            if (products.isEmpty()) {
                result.setMsg("product not found!");
            } else {
                result.setMsg("success");
            }


        } else if (catalog_id == 0) {
            products = productService.findAll();
            for (int i = 0; i < products.size(); i++) {
                Product product = products.get(i);
                product.setImg(product.getPhoto().getImage());
            }

            result.setResult(products);
        }  else if(catalog_id != 0) {
                products = productService.findByCategory(catalog_id);
                for (int i = 0; i < products.size(); i++) {
                    Product product = products.get(i);
                    product.setImg(product.getPhoto().getImage());
                }
                result.setResult(products);
            }

        List<Double> cur = new ArrayList<>();
        Currency.getDollarCurrency();
        for(int i = 0; i<products.size(); i++){
            Product product = products.get(i);
            double price =  Math.round((product.getPriceProduct()/ Currency.getDollarCurrency())*100.0)/100.0 ;
            cur.add(price);
        }
        result.setCurrency(cur);
        /*result.setCurrency(result.getResult().stream().map(product
                -> (double) Math.round((product.getPriceProduct() / Currency.getDollarCurrency()) * 100) / 100)
                .collect(Collectors.toList()));*/

        return ResponseEntity.ok(result);
}
}

