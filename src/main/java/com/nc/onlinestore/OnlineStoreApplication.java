package com.nc.onlinestore;

import com.nc.onlinestore.controller.baseController.MainController;
import com.nc.onlinestore.service.Impl.Currency;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class OnlineStoreApplication {
	private static final Logger log = LoggerFactory.getLogger(MainController.class);

	public static void main(String[] args) {
		SpringApplication.run(OnlineStoreApplication.class, args);

	}

	@PostConstruct
	private void init() {
		log.info("Taking currency with API nat. Bank of Belarus.");
		Currency.updateCurrencyFromNBRBAPI();
		log.info("Starting a timer to update the currency.");
		Currency.startSchedule();
	}
}
