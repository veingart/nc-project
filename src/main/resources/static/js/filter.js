$("input[type='checkbox']").on("click", fire_ajax_submit);
fire_ajax_submit();


function fire_ajax_submit() {

    var filter = $("#my_filter").serialize();
    // $("#btn-search").prop("disabled", true);

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/filter",
        data: JSON.stringify(filter),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {
            var products = data.result;
            var productHtml = "";
            var currency = data.currency;
            var cur = 0;
            products.forEach(function (product) {
                var characteristicHtml = "";
                var i = 0;
                var j = 5;
                var characteristics = product["characteristics"]
                while (i < characteristics.length) {
                    if (i >= j) {
                        break;
                    }
                    characteristicHtml += characteristics[i]["characterictic.nameCharacteristic"] + ": " + characteristics[i]["valueCharacteristic"];
                    if (i === characteristics.length - 1 || i === j - 1) {
                        characteristicHtml += ". ";
                    } else {
                        characteristicHtml += ", ";
                    }

                    i++;
                }

                productHtml +=
                    "    <div class=\"col-md-4 col-xs-6\">\n" +
                    "                    <div class=\"product\">\n" +
                    "                        <div class=\"product-img\">\n" +
                    "                            <a href=\"/catalog/good?productId="+product["id"]+"\">\n" +
                    "                               <img src=\"https://res.cloudinary.com/elmart/" + product["img"] + "\"\n" +
                    "                                     class=\"pd-10 w-100\" height=\"200\">\n" +
                    "                            </a>\n" +
                    "                        </div>\n" +
                    "                        <div class=\"product-body\">\n" +
                    "                            <h2 class=\"product-name\">\n" +
                    "                                <a href=\"/catalog/good?productId="+product["id"]+"\">\n" + product["nameProduct"] +"\n" +
                    "                                </a>\n" +
                    "                            </h2>\n" +
                    "                            <h4 class=\"product-price\">\n" +
                    "                                <label>"+product["priceProduct"]+"РУБ</label>\n" +
                    "                            </h4>\n" +
                    "\n" +
                    "                                            <label class=\"badge badge-primary text-wrap\" style=\"padding: 0.65em .6em;background-color: black;\">"
                    + currency[cur] + " USD</label>\n" +
                    "                            <h3> "+ product["rating"]+"\n" +
                    "                                <i style=\"color: #D10024\" class=\"fa fa-star\"></i></h3>\n" +
                    "                        </div>\n" +
                    "                                                <form method=\"post\" action=\"catalog/add?productId=" + product["id"] + "\" " +
                    "                                              style=\"display:inline;\">" +
                    "                                                  <input type=\"hidden\"  name=\"count\" value=\"1\">" +
                    " <div class=\"add-to-cart\">"+
                    "                                <button class=\"add-to-cart-btn\" type=\"submit\"><i\n" +
                    "                                        class=\"fa fa-shopping-cart\"></i> В\n" +
                    "                                    корзину\n" +
                    "                                </button>\n" +
                    "</div>"+
                    "                            </div>\n" +
                    "                        </form>\n" +
                    "                    </div>\n" +
                    "                </div>";
                cur++;
            });
            $('#feedback').html(productHtml);
            console.log("SUCCESS : ", data);

        },
        error: function (e) {

            var json = "<h4>Ajax Response</h4><pre>"
                + e.responseText + "</pre>";
            $('#feedback').html(json);

            console.log("ERROR : ", e);

        }
    });

}